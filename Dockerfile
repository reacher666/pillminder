FROM python:3.9-alpine


WORKDIR /app

COPY requirements.txt requirements.txt 
RUN pip install -r requirements.txt


COPY run.py run.py

CMD ["python3", "-u", "./run.py"]