import schedule
import os
import requests
import json 
import sys
from requests.auth import HTTPBasicAuth
import time
import signal
import logging as l

try:
    userLevel = os.environ['LOG_LEVEL']
    if userLevel == 'DEBUG':
        level = l.DEBUG
    elif userLevel == 'INFO':
        level = l.INFO
except:
    level = l.INFO
finally:
    l.basicConfig(level=level, format='%(levelname)s - %(asctime)s - %(name)s - %(message)s')


def signal_handler(sig, frame):
    l.info('CTRL+C - Exitting')
    sys.exit(0)

url = os.environ['PROMOSMS_URL']
defaultRecipient = os.environ['PROMOSMS_RECIPIENT']

promoSmsLogin = os.environ['PROMOSMS_LOGIN']
promoSmsPassword = os.environ['PROMOSMS_PASSWORD']
rHeaders = {'Accept': 'text/json'}




def sendMessage(msg, recipient = defaultRecipient):
    checkBallance()
    recipientLst = []
    recipientLst.append(recipient)
    # logMsg('i', repr(msg))
    # return 0
    rData = {   'text': msg,
                'recipients': recipientLst,
                'type': 1,
                'long-sms': 1,
                # 'test': 1
                # 'sender': 'PRZYPOMINAM',
                # 'type': 3,
                # 'special-chars': 1
    }

    r = requests.put(   url, 
                        data=rData,
                        headers=rHeaders,
                        auth=HTTPBasicAuth(promoSmsLogin, promoSmsPassword)
                    )

    response = json.loads(r.text)['response']
    apiStatus = response['status']
    sentSmsCount = response["sentSmsCount"]

    if r.status_code != 200:
        l.error(f'{r.status_code}, status="httpError", response="{r.text}"')
        return r.status_code
    #response={"response":{"status":0,"price":0,"sentSmsCount":1,"contentId":null,"recipientsResults":[]}}
    elif apiStatus == 0:
        l.info(f'httpCode="{r.status_code}", status="sent", sentSmsCount="{sentSmsCount}", msg="{repr(msg)}"')
        return apiStatus
    elif apiStatus != 0:
        l.error(f'httpCode="{r.status_code}", status="failed", response="{response}", msg="{repr(msg)}')
        return apiStatus

    else:
        l.error(f'httpCode="{r.status_code}", status="uncaught error", response="{r.text}", msg="{repr(msg)}')
        return r.status_code

def checkBallance(counter = 0, sleep=False):
    if sleep:
        time.sleep(5)
    url = 'https://promosms.com/api/rest/v3_2/balance'

    if counter < 5:
        try:
            r = requests.get(   url, 
                                headers=rHeaders,
                                auth=HTTPBasicAuth(promoSmsLogin, promoSmsPassword)
                            )
            response = json.loads(r.text)["response"]
            if r.status_code == 200 and response['status'] == 0:
                l.info(f'SMSAPI connected, response={response}')
                counter = 0
                if response['balanceAvailable'] == 0:
                    l.error(f'Balance = 0, Retry in 5s, response={response}')
                    checkBallance(counter+1, True)
            else:
                if counter <= 5:
                    l.error(f'SMSAPI CONNECTION ERROR, Retry in 5s, response={response}')
                    checkBallance(counter+1, True)
        except:
            if counter < 5:
                l.error('HTTP connection error to PROMOSMS. Retry in 5s')
                checkBallance(counter+1, True)
    else:
        l.error('TOO MANY ATTEMPTS. BYE.')
        sys.exit(1)

signal.signal(signal.SIGINT, signal_handler)
checkBallance()

for msg in json.loads(os.environ['MESSAGES']):
    schedule.every().day.at(msg['time']).do(sendMessage, msg=msg['text'])    

schedules = schedule.get_jobs()
for key in schedules:
    l.info(f'Job configured="{key}"')

sendMessage(f'Pillminder started!', os.environ['MAINTAINER_NUMBER'])

# schedule.run_all()
while True:
    schedule.run_pending()
    time.sleep(1)