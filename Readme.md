# Pillminder
This app was initially designed to send SMS with Promosms reminding to take pills, but can be used for sending any SMSes daily.

# Building
`docker build . -t pillminder`

# Running
1. Rename docker-compose.yml.smp to docker-compose.yml and write your SMSes.
1. Rename docker.env.smp to docker.env and set your numbers...
1. `docker-compose up -d` 